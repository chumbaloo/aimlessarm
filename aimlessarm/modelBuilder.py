#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

""" Doc string for module """

# Imports
import os
import theano
import pickle
import logging
import theano.tensor as T
import numpy as np
import aimlessarm.layers as template


class ModelBuilder(object):

    def __init__(self, x, y, units=None, layers=None, activations=None, rng=np.random.RandomState(0),
                 logfilename="model.log", lambda_1=0, lambda_2=0, disable_logging=False, **kwargs):

        self.__qualname__ = "ModelBuilder"

        # Check inputs
        assert(isinstance(x, theano.tensor.TensorVariable))
        assert(isinstance(y, theano.tensor.TensorVariable))

        # Check sufficient units in list
        assert(len(units) == len(layers))

        # Ensure layers have right number of activations, 1 per layer
        assert(len(activations) == len(layers))

        # Create logger object
        self.logger = logging.getLogger(self.__qualname__)
        self.logger.setLevel(logging.DEBUG)

        # Console stream
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        self.logger.addHandler(ch)

        # File stream
        self.logfilename = logfilename
        fh = logging.FileHandler(self.logfilename)
        fh.setLevel(logging.DEBUG)

        if not disable_logging:
            self.logger.addHandler(fh)

        # Assign to class variables
        self.x = x
        self.y = y

        # Check to see if dropout enable is required
        if 'DropoutLayer' in layers:
            self.enable_drop = T.bscalar()

        self.units = units
        self.rng = rng
        basename, ext = os.path.splitext(self.logfilename)
        self.weights_filename = self.logfilename.replace(ext, '.pkl')
        self.activations = activations
        self.lambda_1 = lambda_1
        self.lambda_2 = lambda_2

        self._build(layers, activations, self.rng, **kwargs)

    def _build(self, layers, activations, rng, **kwargs):
        """ Build the model """

        # Construct the layers
        self.layers = []
        self.params = []

        # Handle additional arguments
        extra_args = {}
        if kwargs and ('extra_args' in kwargs):
            extra_args = kwargs['extra_args']

        # Construct the first layer - the input layer
        for idx, (layer, activation) in enumerate(zip(layers, activations)):

            # Get the appropriate layer
            layer = getattr(template, layer)
            layer_args = {}

            # Control input layer tensor
            if layer.__qualname__ == 'InputLayer':
                layer_args['tensor_in'] = self.x
                layer_args['input_shape'] = self.units[idx]
            else:
                layer_args['tensor_in'] = self.layers[-1].output
                layer_args['input_shape'] = self.layers[-1].output_shape

            layer_args['output_shape'] = self.units[idx]
            layer_args['activation'] = activation
            layer_args['rng'] = rng

            if hasattr(self, 'enable_drop'):
                layer_args['enable'] = self.enable_drop

            # Construct layer args
            if idx in extra_args:
                layer_args.update(extra_args[idx])

            layer = layer(**layer_args)

            self.layers.append(layer)

            # Some layers e.g. pooling do not have weights
            if hasattr(layer, 'params'):
                self.params += layer.params

        self.output = self.layers[-1].output

        if hasattr(self, 'enable_drop'):
            predict_givens = {
                self.enable_drop: np.int8(False),
            }
        else:
            predict_givens = {}

        self.predict = theano.function(
            inputs=[self.x],
            outputs=self.output,
            givens=predict_givens,
        )

        # Replace this with alternative cost functions if necessary
        cost = T.mean(T.sqrt((self.output - self.y)**2))

        self.construct_cost(cost)

    def construct_cost(self, cost_tensor):
        """ Construct the cost function with regularisation """

        # Add regularisation
        L1 = 0
        for param in self.params:
            L1 += T.sum(abs(param))

        L2 = 0
        for param in self.params:
            L2 += T.sum(param ** 2)

        self.cost = cost_tensor + self.lambda_1 * L1 + self.lambda_2 * L2
        self._construct_grads()

    def _construct_grads(self):

        # Grads
        self.grads = [T.grad(self.cost, param) for param in self.params]

    def save(self, params, train_hist=None):
        """Save the weights to a pickle file for later use"""

        model_info = {
            'units': self.units,
            'layers': [template.get_name(layer) for layer in self.layers],
            'activations': [template.get_name(act) for act in self.activations],
            'weights': [param for param in params],
        }

        if train_hist:
            model_info['train_hist'] = np.asarray(train_hist)

        with open(self.weights_filename, 'wb') as f:
            pickle.dump(model_info, f)

    def load(self, filename):
        """ Load the weights from a pickle file """

        with open(filename, 'rb') as f:
            saved_info = pickle.load(f)

        # Update weights
        for param, value in zip(self.params, saved_info['weights']):
            param.set_value(value)

    def __str__(self):
        """Print model to screen"""

        msg = ""

        for idx, layer in enumerate(self.layers):
            msg += "{:<10}{}\n".format(
                "Layer %i:" % (idx + 1),
                str(layer))

        msg += "lambda_1: {:E}\n".format(self.lambda_1)
        msg += "lambda_2: {:E}\n".format(self.lambda_2)
        msg += "Logfile: %s\n" % self.logfilename
        msg += "Weights File: %s" % self.weights_filename
        return msg
