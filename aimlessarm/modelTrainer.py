#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

""" Doc string for module """

# Imports
import os
from copy import copy
import theano
import theano.tensor as T
import numpy as np


class FixedValue(object):
    """ Base Class for Fixed Value Parameters"""

    def __init__(self, value):
        self.value = value

    def __call__(self):
        return self.value


class FixedMomentum(FixedValue):

    def __str__(self):
        return "Fixed Momentum: {:E}".format(self.value)


class FixedLearnRate(FixedValue):

    def __str__(self):
        return "Fixed Learning Rate: {:E}".format(self.value)


class ScheduledValue(object):
    """ Base Class For Scheduling Values """

    def __init__(self, rates, schedules):
        self.rates = rates
        self.initial_rates = copy(rates)
        self.schedules = schedules
        self.initial_schedules = copy(schedules)
        self.counter = 0

        self._update()

    def _update(self):
        if (len(self.rates) > 0) and (len(self.schedules) > 0):
            self.curr_rate = self.rates.pop(0)
            self.curr_sched = self.schedules.pop(0)

    def __call__(self):

        if self.counter >= self.curr_sched:
            self._update()
        self.counter += 1
        return self.curr_rate


class ScheduledMomentum(ScheduledValue):

    def __str__(self):
        return "Scheduled Momentum: {} @ {}".format(self.initial_rates,
                                                    self.initial_schedules)


class ScheduledLearnRate(ScheduledValue):

    def __str__(self):
        return "Scheduled Learning Rate: {} @ {}".format(self.initial_rates,
                                                         self.initial_schedules)


defaultMomentum = FixedMomentum(0.9)
defaultEta = FixedLearnRate(0.01)


class ModelTrainer(object):

    def __str__(self):

        msg = "Training Data:\n"
        msg += "\txtrain shape: {}\n".format(self.xtrain.eval().shape)
        msg += "\tytrain shape: {}\n".format(self.ytrain.eval().shape)
        msg += "Validation Data:\n"
        msg += "\txvalid shape: {}\n".format(self.xvalid.eval().shape)
        msg += "\tyvalid shape: {}\n".format(self.yvalid.eval().shape)
        msg += "{}\n".format(self.eta)
        msg += "{}\n".format(self.momentum)
        msg += "Batch Size: {}\n".format(self.batch_size)
        msg += "Patience: {}\n".format(self.patience)
        msg += "Max Epochs: {}\n".format(self.max_epochs)
        return msg

    def __init__(self,
                 model,
                 train_data,
                 eta=defaultEta,
                 momentum=defaultMomentum,
                 patience=50,
                 max_epochs=np.inf,
                 batch_size=None,
                 save_weights=True,
                 ):
        """ Initialise the trainer """

        # Stick to scikit learn convention
        self.xtrain, self.xvalid, self.ytrain, self.yvalid = train_data

        # Save parameters
        self.eta = eta
        self.momentum = momentum
        self.patience = patience
        self.max_epochs = max_epochs
        self.save_weights = save_weights

        if isinstance(self.xtrain, theano.tensor.sharedvar.TensorSharedVariable):
            num_train = self.xtrain.get_value(borrow=True).shape[0]
        else:
            num_train = self.xtrain.eval().shape[0]

        # Manage use of mini-batches
        idx = T.iscalar()
        self.batch_size = batch_size
        if self.batch_size is not None:
            self.num_train_batches = num_train // batch_size
            self.train_batch_size = batch_size
        else:
            self.num_train_batches = 1
            self.train_batch_size = num_train

        # Get some references from the model
        self.model = model
        self.x = model.x
        self.y = model.y
        self.params = model.params
        self.cost = model.cost
        self.grads = model.grads
        self.output = model.output
        self.logger = model.logger

        # Generate updates
        self._updates()

        # Generate the functions
        # Prepare training function
        self.train_givens = {
            model.x: self.xtrain[idx * self.train_batch_size: (idx + 1) * self.train_batch_size],
            model.y: self.ytrain[idx * self.train_batch_size: (idx + 1) * self.train_batch_size],
        }
        if hasattr(model, 'enable_drop'):
            self.train_givens[model.enable_drop] = 1

        self.f_train = theano.function(
            inputs=[idx],
            outputs=[self.cost],
            updates=self.updates,
            givens=self.train_givens,
        )

        # Prepare validation set functions
        self.valid_givens = {
            model.x: self.xtrain,
            model.y: self.ytrain,
        }
        if hasattr(model, 'enable_drop'):
            self.valid_givens[model.enable_drop] = 0

        self.f_valid = theano.function(
            inputs=[],
            outputs=self.cost,
            givens=self.valid_givens,
        )

    def _updates(self):
        """ Updates - overload to change update method"""

        self.updates = []
        for param in self.params:
            previous_step = theano.shared(
                param.get_value() * 0., broadcastable=param.broadcastable)
            step = self.momentum() * previous_step - self.eta() * T.grad(self.cost, param)
            self.updates.append((previous_step, step))
            self.updates.append((param, param + step))

    def train(self):
        """ Train """

        self.logger.info("Model Architecture:")
        self.logger.info(self.model)
        self.logger.info(self)

        best_valid_cost = np.inf
        prev_best_iter = 0
        best_params = []
        i = 0

        train_hist = []

        while 1:

            train_cost = np.mean([self.f_train(j)
                                  for j in range(self.num_train_batches)])
            valid_cost = np.mean(self.f_valid())

            train_hist.append([train_cost, valid_cost])

            self.logger.info(
                "{:d}: train error: {:0.6f} valid error: {:0.6f}"
                " valid/train: {:0.6f} improvement {}".format(
                    i, train_cost, valid_cost,
                    valid_cost / train_cost, valid_cost < best_valid_cost,
                )
            )

            if valid_cost < best_valid_cost:
                prev_best_iter = i
                best_valid_cost = valid_cost
                best_params = [param.get_value() for param in self.params]

            elif (i > (prev_best_iter + self.patience)):
                break

            i = i + 1

            if (i >= self.max_epochs):
                break

        self.logger.info(
            "Best valid error: {:0.6f} @ {}".format(
                best_valid_cost, prev_best_iter))
        # We have finished save the results
        if self.save_weights:
            self.model.save(best_params, train_hist)
