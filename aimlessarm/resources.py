#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Module for loading MNIST data



:author: Ben Johnston

"""

# Imports
import os
import gzip
import theano
import numpy as np
import theano.tensor as T
from urllib.request import urlretrieve
from aimlessarm.modelBuilder import ModelBuilder
from aimlessarm.modelTrainer import ModelTrainer

MNIST_TRAIN_IMAGES = 'train-images-idx3-ubyte.gz'
MNIST_TRAIN_LABELS = 'train-labels-idx3-ubyte.gz'


def load_MNIST(folder_path=os.path.dirname(__file__)):
    """ Load the MNIST dataset

    This function loads the MNIST dataset as 
    published by Yann LeCun, Corinna Cortes and
    Christopher J.C Burges (yann.lecun.com/exdb/mnist/).
    If the dataset has already been saved locally, the local
    copy will be read, if the dataset will be downloaded, stored
    and data returned.

    Parameters
    ----------

    folder_path: str
        The folder path to the locally stored MNIST dataset. If MNIST
        is not already locally stored, it will be downloaded to this path

    Returns
    _______

    data: 
    """

    def _read(bytestream):
        dt = np.dtype(np.uint32).newbyteorder('>')
        return np.frombuffer(bytestream.read(4), dtype=dt)[0]

    def read_images(filename):
        """ Read training images """

        with gzip.open(filename, 'rb') as bytestream:
            magic = _read(bytestream)

            # Check magic number
            if magic != 2051:
                raise ValueError('Invalid Magic Number %d in %s' %
                                 (magic, filename))

            num_images = _read(bytestream)
            rows = _read(bytestream)
            cols = _read(bytestream)
            buf = bytestream.read(num_images * rows * cols)
            data = np.frombuffer(buf, dtype=np.uint8)
            data = data.reshape(num_images, rows, cols)
            data = data.astype(np.float32)

        return data

    def read_labels(filename):
        """ Read training labels """

        with gzip.open(filename, 'rb') as bytestream:
            magic = _read(bytestream)

            # Check magic number
            if magic != 2049:
                raise ValueError('Invalid Magic Number %d in %s' %
                                 (magic, filename))

            num_labels = _read(bytestream)
            buf = bytestream.read(num_labels)
            data = np.frombuffer(buf, dtype=np.uint8)

            # Return with one hot encoding
            encoding = np.zeros((num_labels, 10))
            for idx, label in enumerate(data):
                encoding[idx, label] = 1
                encoding = encoding.astype(np.int32)

        return encoding

    # Search path for the MNIST training files
    files_in_path = [filename for filename in os.listdir(folder_path)]

    # If the files dont exist, download them
    images_file = os.path.join(folder_path, MNIST_TRAIN_IMAGES)
    labels_file = os.path.join(folder_path, MNIST_TRAIN_LABELS)
    if not MNIST_TRAIN_IMAGES in files_in_path:
        print("Downloading MNIST training images")
        urlretrieve("http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz",
                    images_file)

    if not MNIST_TRAIN_LABELS in files_in_path:
        print("Downloading MNIST training labels")
        urlretrieve("http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz",
                    labels_file)

    images = read_images(images_file)
    labels = read_labels(labels_file)

    return images, labels


def load_train_data(folder_path=os.path.dirname(__file__)):
    """ Load MNIST model """

    print("Loading Data")

    images, labels = load_MNIST(folder_path)
    num_train = int(0.7 * len(images))
    num_valid = int(0.9 * len(images))

    images -= np.mean(images)
    images = images / 255

    x_train = images[:num_train]
    y_train = labels[:num_train]

    x_valid = images[num_train:num_valid]
    y_valid = labels[num_train:num_valid]

    x_test = images[num_valid:]
    y_test = labels[num_valid:]

    xtrain = theano.shared(
        np.asarray(x_train, dtype=theano.config.floatX),
        borrow=True)

    ytrain = theano.shared(
        np.asarray(y_train),
        borrow=True)

    xvalid = theano.shared(
        np.asarray(x_valid, dtype=theano.config.floatX),
        borrow=True)

    yvalid = theano.shared(
        np.asarray(y_valid),
        borrow=True)

    return xtrain, xvalid, x_test, ytrain, yvalid, y_test


def modelTrain(model, train_data):
    """ Train the model -limit number of epochs for now"""

    xtrain, xvalid, x_test, ytrain, yvalid, y_test = train_data

    trainer = ModelTrainer(
        model,
        [xtrain, xvalid, ytrain, yvalid],
        batch_size=20,
        max_epochs=10,
        save_weights=False,
    )

    # Train
    trainer.train()

    # Print accuracy
    predictions = model.predict(x_test)
    predictions = np.argmax(predictions, axis=1)
    actuals = np.argmax(y_test, axis=1)
    accuracy = np.sum(predictions == actuals) / y_test.shape[0]

    print("Accuracy: %0.2f%%" % (accuracy * 100))


def mlpMNIST(folder_path=os.path.dirname(__file__)):
    """ Multi Layer Perceptron - produces model with approx 96.72% accuracy"""

    xtrain, xvalid, x_test, ytrain, yvalid, y_test = load_train_data(
        folder_path)
    xtrain = xtrain.flatten((2))
    xvalid = xvalid.flatten((2))

    x_test = x_test.reshape((x_test.shape[0], -1))

    # Modify
    x = T.matrix()
    y = T.imatrix()

    print("Building Model")

    # Build the model
    rng = np.random.RandomState(0)
    model = ModelBuilder(
        x, y,
        units=[28 * 28, 500, 10],
        layers=['InputLayer', 'HiddenLayer', 'OutputLayer'],
        activations=[None, T.tanh, T.nnet.softmax],
        rng=rng,
        lambda_2=0.001,
    )

    loglikelihood = -T.mean(T.log(model.output)
                            [T.arange(T.shape(model.y)[0]), T.argmax(model.y, axis=1)])
    model.construct_cost(loglikelihood)

    modelTrain(model, [xtrain, xvalid, x_test, ytrain, yvalid, y_test])


def alexMNIST(folder_path=os.path.dirname(__file__)):
    """ Alex-net ish CNN - produces model with approx 98.6% accuracy"""

    xtrain, xvalid, x_test, ytrain, yvalid, y_test = load_train_data(
        folder_path)
    xtrain = xtrain.dimshuffle(0, 'x', 1, 2)
    xvalid = xvalid.dimshuffle(0, 'x', 1, 2)
    x_test = x_test.reshape((-1, 1, 28, 28))

    # Modify
    x = T.tensor4()
    y = T.imatrix()

    print("Building Model")

    # Build the model
    rng = np.random.RandomState(0)

    # Filter args
    n_filters = [20, 50]
    kwargs = {}
    kwargs[1] = {'filter_shape': [n_filters[0], 1, 5, 5]}
    kwargs[3] = {'filter_shape': [n_filters[1], n_filters[0], 5, 5]}

    model = ModelBuilder(
        x, y,
        units=[
            [None, 1, 28, 28],  # Input Layer
            [None, n_filters[0], 24, 24],  # Cov Layer 1 (28 - 5) + 1 = 24
            [None, n_filters[0], 12, 12],  # Pool Layer 1 24 // 2 = 12
            [None, n_filters[1], 8, 8],  # Cov Layer 2 (12 - 5) + 1 = 8
            [None, n_filters[1], 4, 4],  # Pool Layer 2 8 // 2 = 4
            500,  # Hidden Layer
            10,  # Output Layer
        ],
        layers=[
            'InputLayer',
            'ConvLayer',
            'PoolLayer',
            'ConvLayer',
            'PoolLayer',
            'HiddenLayer',
            'OutputLayer',
        ],
        activations=[
            None,
            T.tanh,
            None,
            T.tanh,
            None,
            T.tanh,
            T.nnet.softmax,
        ],
        rng=rng,
        lambda_2=0.001,
        extra_args=kwargs,
    )

    loglikelihood = -T.mean(T.log(model.output)
                            [T.arange(T.shape(model.y)[0]), T.argmax(model.y, axis=1)])
    model.construct_cost(loglikelihood)

    modelTrain(model, [xtrain, xvalid, x_test, ytrain, yvalid, y_test])


if __name__ == "__main__":
    # mlpMNIST()
    alexMNIST()
