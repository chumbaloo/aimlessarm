#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages
import versioneer

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click>=6.0',
    'numpy>=1.13.3',
    'theano>=1.0.1',
]

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest>=3.3.1',
    'pytest-cov>=2.5.1',
    'pytest-mock>=1.6.3',
    'pytest-watch>=4.1.0',
]

setup(
    name='aimlessarm',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Templated neural network library using Theano",
    long_description=readme + '\n\n' + history,
    author="Ben Johnston",
    author_email='bjohnston@neomailbox.net',
    url='https://github.com/doc-E-brown/aimlessarm',
    packages=find_packages(include=['aimlessarm']),
    entry_points={
        'console_scripts': [
            'aimlessarm=aimlessarm.cli:main'
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords='aimlessarm',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
#        "Programming Language :: Python :: 2",
#        'Programming Language :: Python :: 2.6',
#        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='pytest',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
