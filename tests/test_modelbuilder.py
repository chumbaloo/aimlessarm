#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

""" Test model builder """

# Imports
import os
import pytest
import theano
import logging
import numpy as np
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from unittest.mock import patch, mock_open, MagicMock
from aimlessarm.modelBuilder import ModelBuilder
from aimlessarm.layers import BaseLayer, HiddenLayer,\
    ConvLayer, PoolLayer, InputLayer, OutputLayer
from .test_layers import MockRandomState,\
    activation

# Generic ModelBuilder Tests ################################


def test_ModelBuilder_init():
    """Test initialisation of model builder - one HiddenLayer"""

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 3]
    activations = [None, activation, None]
    layer_spec = ['InputLayer', 'HiddenLayer', 'OutputLayer']

    rng = MockRandomState(1)
    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=rng,
                         disable_logging=True,
                         )

    # Check the layer structure
    for idx, layer in enumerate(model.layers):
        assert(layer.__qualname__ == layer_spec[idx])
        if idx == 0:
            assert(layer.input_shape == [units[idx]])
        else:
            assert(layer.input_shape == [units[idx - 1]])
        assert(layer.output_shape == [units[idx]])
        assert(layer.activation == activations[idx])

    assert(len(model.params) == 4)

    # Check the string representation
    expected_str = "{:<10}{:<10}: [1] {:<10}None\n".format(
        'Layer 1:', 'InputLayer', 'Activation: ')
    expected_str += "{:<10}{:<10}: [1]x[2] {:<10}activation\n".format(
        'Layer 2:', 'HiddenLayer', 'Activation: ')
    expected_str += "{:<10}{:<10}: [2]x[3] {:<10}None\n".format(
        'Layer 3:', 'OutputLayer', 'Activation: ')
    expected_str += "lambda_1: 0.000000E+00\n"
    expected_str += "lambda_2: 0.000000E+00\n"
    expected_str += "Logfile: model.log\nWeights File: model.pkl"

    assert(str(model) == expected_str)


def test_modelbuilder_enable_logging():
    """ Test enable logging """

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 3]
    activations = [None, activation, None]
    layer_spec = ['InputLayer', 'HiddenLayer', 'OutputLayer']

    rng = MockRandomState(1)
    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=rng,
                         disable_logging=False,
                         )
    assert(os.path.exists(model.logfilename))
    os.remove(model.logfilename)


def test_save_model(mocker):
    """Test saving the model"""

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 3]
    layer_spec = ['InputLayer', 'HiddenLayer', 'OutputLayer']
    activations = [None, activation, None]

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=MockRandomState(1),
                         logfilename='test.pkl',
                         disable_logging=True,
                         )

    m = mock_open()
    pickle_mock = mocker.patch('pickle.dump')
    with patch('builtins.open', m):
        model.save([param.get_value() for param in model.params])

    m.assert_called_with('test.pkl', 'wb')

    pickle_dict = pickle_mock.call_args[0][0]

    assert(pickle_dict['units'] == units)
    assert(pickle_dict['layers'] == layer_spec)
    assert(pickle_dict['activations'] == ['None', 'activation', 'None'])

    expected_weights = [
        np.ones((1, 2), dtype=theano.config.floatX),
        np.ones((2,), dtype=theano.config.floatX),
        np.ones((2, 3), dtype=theano.config.floatX),
        np.ones((3,), dtype=theano.config.floatX),
    ]

    np.testing.assert_equal(
        pickle_dict['weights'], expected_weights)

    assert(pickle_mock.call_args[0][1] == m())


def test_save_model_train_hist(mocker):
    """Test saving the model with training history"""

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 3]
    layer_spec = ['InputLayer', 'HiddenLayer', 'OutputLayer']
    activations = [None, activation, None]

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=MockRandomState(1),
                         logfilename='test.pkl',
                         disable_logging=True,
                         )

    m = mock_open()
    pickle_mock = mocker.patch('pickle.dump')
    train_hist = [1, 2, 3, 4, 5]
    with patch('builtins.open', m):
        model.save([param.get_value() for param in model.params],
                   train_hist)

    pickle_dict = pickle_mock.call_args[0][0]
    assert(np.all(pickle_dict['train_hist'] == train_hist))


def test_load_model(mocker):
    """Test loading the model"""

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 3]
    layer_spec = ['InputLayer', 'HiddenLayer', 'OutputLayer']
    activations = [None, activation, None]

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=MockRandomState(1),
                         logfilename='test.pkl',
                         disable_logging=True,
                         )

    expected_weights = [
        np.ones((2, 3), dtype=theano.config.floatX),
        np.ones((4,), dtype=theano.config.floatX),
        np.ones((5, 6), dtype=theano.config.floatX),
        np.ones((7,), dtype=theano.config.floatX),
    ]

    m = mock_open()
    pickle_mock = mocker.patch('pickle.load',
                               return_value={'weights': expected_weights})
    with patch('builtins.open', m):
        model.load('test.pkl')

    m.assert_called_with('test.pkl', 'rb')

    # Check loaded weights
    for param, expected in zip(model.params, expected_weights):
        np.testing.assert_almost_equal(param.eval(), expected)


# Convolutional Models ##########################################
def conv_pool_model():
    """Test a model with a single convolutional & pool layer"""

    x = T.tensor4()
    y = T.matrix()

    filters = [
        None,
        [3, 1, 2, 2],  # Filters for CNN layer
        None,
    ]

    units = [
        [None, 1, 3, 3],  # Input Image
        [None, 3, 3, 3],  # Input to CNN layer
        [None, 3, 1, 1],  # Output
    ]

    layer_spec = ['ConvLayer', 'PoolLayer']
    activations = [None, None, None]
    rng = MockRandomState(0.5)

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=rng,
                         weights_path='test.pkl',
                         disable_logging=True,
                         )

    assert(isinstance(model.layers[0], ConvLayer))
    assert(isinstance(model.layers[1], PoolLayer))

    # Params should only be ConvLayer params
    assert(model.params == model.layers[0].params)

    # Check the output
    input_arr = np.array([[[
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]]])

    expected_output = np.array([
        [
            [14.5],
        ],
        [
            [14.5],
        ],
        [
            [14.5],
        ],
    ])

    output = model.predict(input_arr)
    assert(np.all(output == expected_output))


def test_conv_pool_output_model():
    """ Test a CNN with a pool and nnet output layer """

    x = T.tensor4()
    y = T.matrix()

    input_arr = np.array([[[
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]]])

    units = [
        [None, 1, 3, 3],  # None batches * 1 * 3 *3 image,
        [None, 3, 2, 2],  # None batches * 3 x 2 x 2
        [None, 3, 1, 1],  # After pooling
        3,  # Output layer
    ]

    kwargs = {}
    kwargs[1] = {'filter_shape': [3, 1, 2, 2]}

    layer_spec = ['InputLayer', 'ConvLayer', 'PoolLayer', 'OutputLayer']
    activations = [None, None, None, None]
    rng = MockRandomState(0.5)

    model = ModelBuilder(
        x, y,
        units=units,
        layers=layer_spec,
        activations=activations,
        rng=rng,
        weights_path='test.pkl',
        disable_logging=True,
        extra_args=kwargs
    )

    assert(isinstance(model.layers[0], InputLayer))
    assert(isinstance(model.layers[1], ConvLayer))
    assert(isinstance(model.layers[2], PoolLayer))
    assert(isinstance(model.layers[3], OutputLayer))

    # 2 for ConvLayer, 2 for OutputLayer
    assert(len(model.params) == 4)

    # Check the output
    expected_output = np.array([[22.25, 22.25, 22.25]])

    output = model.predict(input_arr)
    assert(np.all(output == expected_output))


def test_dropout_layer_model():
    """ Test dropout layer functionality """

    x = T.matrix()
    y = T.vector()

    units = [1, 2, 2, 3]

    layer_spec = ['InputLayer', 'HiddenLayer',
                  'DropoutLayer', 'OutputLayer']

    activations = [None, activation, None, None]

    kwargs = {}
    kwargs[2] = {'p': 0.5}

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layer_spec,
                         activations=activations,
                         rng=np.random.RandomState(0),
                         logfilename='test.pkl',
                         disable_logging=True,
                         extra_args=kwargs,
                         )

    assert(hasattr(model, 'enable_drop'))
    dropout_output = model.layers[2].output.eval({
        model.x: np.ones((1, 1)),
        model.enable_drop: 1})
    assert(np.any(dropout_output == 0))
